// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCsHk4A01EAEFSZ2p4n4vPdqdff8p235rU",
    authDomain: "bbc-project-c6d34.firebaseapp.com",
    databaseURL: "https://bbc-project-c6d34.firebaseio.com",
    projectId: "bbc-project-c6d34",
    storageBucket: "bbc-project-c6d34.appspot.com",
    messagingSenderId: "267658553626",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
