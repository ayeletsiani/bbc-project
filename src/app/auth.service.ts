import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { User } from './interfaces/user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  massegse:string;
  user: Observable<User | null>
  logInErrorSubject = new Subject<string>();
  public masageerror;

  constructor(public afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState //פייר בייס ינהל את היוזרים
    
  }

  signUp(email:string, password:string)
  {
    this.afAuth
        .auth.createUserWithEmailAndPassword(email,password)
        .then(() => { 
          this.router.navigate(['/welcome']);
        }) //"then" הפונקציה 
        .catch(error => this.masageerror = error
          )
                                                            //מחזירה פורמיס
                                                            // כמו סאבסקרייב של אובזראבל
  this.massegse = "Sign up successfully";
                                                          }

 logOut(){ 
   console.log("Succesful Logout");
   this.massegse = "Succesful Logout";
   this.afAuth.auth.signOut().then(() => {
                                           this.router.navigate(['/welcome']);})
  
 }

 login(email:string,password:string)
 {
    console.log(email);
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .then(
          () => this.router.navigate(['/welcome'])
        ).catch(error => this.masageerror = error
          )
     this.massegse = "Succesful Login";   
  }

  public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;
  }

  public getUser():Observable<User>{
    return this.user;
  }
}
