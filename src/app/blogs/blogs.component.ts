import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';
import { Blog } from '../interfaces/blog';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Comments } from '../interfaces/comments';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {

  blogs$:Blog[]=[];
  comments$:Comments[]=[];
  success:string="";
  blogCollection:AngularFirestoreCollection;
  afterSave:string = "Saved for later viewing";
  saved:boolean = false;
  savedId:number[]=[];

  constructor(public blogservice:BlogService, private db:AngularFirestore) { }

  ngOnInit() {
    this.blogservice.getComments()
         .subscribe(data =>this.comments$ = data );
    this.blogservice.getBlogs()
          .subscribe(data =>this.blogs$ = data);
      }

  saveBlog(blogId:number,title:string,body:string)
    {
      this.blogservice.addBlog(blogId,title,body);
      let i = this.savedId.length;
      this.savedId[i++]= blogId;
      console.log(this.savedId);
    } 
}
