import { Component, OnInit } from '@angular/core';
import { Blog } from '../interfaces/blog';
import { BlogService } from '../blog.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-saved',
  templateUrl: './saved.component.html',
  styleUrls: ['./saved.component.css']
})
export class SavedComponent implements OnInit {

  mypost$:Observable<any>;
  userId:string;
  constructor(private blogservice:BlogService, private db:AngularFirestore, public auth:AuthService) { }

  ngOnInit() {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.mypost$ = this.blogservice.getMyPosts(this.userId);
      }
    )
    
  }

  deletePost(postId:number){
    this.blogservice.deletePost(this.userId,postId);
  }


  likeUp(postId:number,title:string,body:string, like:number){
      this.blogservice.addLike(this.userId,postId,title,body,like);

  }


}
