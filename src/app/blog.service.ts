import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {HttpClient, HttpClientModule } from '@angular/common/http';  
import { map, catchError } from 'rxjs/operators';
import { Blog } from './interfaces/blog';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Comments } from './interfaces/comments';
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class BlogService {
  
  URLB:string ="https://jsonplaceholder.typicode.com/posts/";
  URLcoment :string ="https://jsonplaceholder.typicode.com/comments";
  userCollection:AngularFirestoreCollection= this.db.collection('users');
  blogCollection:AngularFirestoreCollection;
  userId;
  successMessage:string;
  
  constructor(private http:HttpClient, private db:AngularFirestore, public auth:AuthService) 
  {
     this.userId = this.auth.getUser().subscribe(
      res=> this.userId = res.uid
    );}


  getBlogs(){
    return this.http.get<Blog[]>(this.URLB);
    
  }
  
  getComments(){
    
    return this.http.get<Comments[]>(this.URLcoment);
    
  }

  addBlog(blogId:number,title:string,body:string){
    const blog = {id: blogId, title: title, body:body, like:0};
    console.log(blog);
    this.userCollection.doc(this.userId).collection('blogs').add(blog);
    console.log("Added Successfully");
  }
 
  getMyPosts(userId:string):Observable<any[]>{
    this.blogCollection = this.db.collection(`users/${userId}/blogs`);
    return this.blogCollection.snapshotChanges().pipe(
      map(
       collection => collection.map(
         document=> {
           const data = document.payload.doc.data();
           data.id = document.payload.doc.id;
           return data;
         }
       ) 
      )
    )
   }

   deletePost(userId:string, postId:number){
    this.db.doc(`users/${userId}/blogs/${postId}`).delete();
    console.log("Deleted Successfully");
   }
 

   addLike(userId:string, postId:number,title:string,body:string, like:number){
    const likeNew = like+1;
    const blog = {id: postId, title:title, body:body, like:likeNew};
    this.db.doc(`users/${userId}/blogs/${postId}`).update(blog);
   }
        
   

}
