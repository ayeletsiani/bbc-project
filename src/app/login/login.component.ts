import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  

  hide = true;
  email:string;
  password:string; 
  public errorMessege;

  constructor(public auth:AuthService, private route:ActivatedRoute,
    private router:Router) {
      this.auth.getLoginErrors().subscribe( error=> {
        this.errorMessege = error;
       });
       auth.masageerror="";
     }
     
  onSubmit(){
    this.auth.login(this.email,this.password);
    
    
  }

  ngOnInit() {
  }


}

