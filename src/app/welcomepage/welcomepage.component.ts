import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { User } from 'firebase';

@Component({
  selector: 'app-welcomepage',
  templateUrl: './welcomepage.component.html',
  styleUrls: ['./welcomepage.component.css']
})
export class WelcomepageComponent implements OnInit {

  title:string;
  email;
  constructor(public auth:AuthService) { }

  ngOnInit() {
    this.email = this.auth.getUser().subscribe((res)=>
      this.email = res.email);
  }

}
