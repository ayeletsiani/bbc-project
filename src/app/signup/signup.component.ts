import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  hide = true;
  email:string;
  password:string;  
  public errorMessege;

  constructor(public auth: AuthService, private router: Router, private route:ActivatedRoute) { 
   this.auth.getLoginErrors().subscribe( error=> {
    this.errorMessege = error;
   });
   auth.masageerror="";
  }

  onSubmit(){
    this.auth.signUp(this.email,this.password);
  }
  
  ngOnInit() {
  }

}
